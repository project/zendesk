# Zendesk Remote Authentication

The zendesk module allows you to integrate your Drupal site with the zendesk
customer support system (http://www.zendesk.com).

Zendesk allows external authentication. This means that your Drupal site can
serve as authentication service. This way customers have a seamless experience
without having to log in to both the Drupal site and the Zendesk support system.

## Requirements

This module requires the Zendesk PHP library:
http://code.google.com/p/zendesk-php-lib.

This module requires no modules outside of Drupal core.

## Installation

This module must be installed with Composer. For further information, see
[Installing Drupal
Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Enable the module at Administration > Extend.

2. On the Zendesk site, configure Zendesk's API tokens at Apps and integrations
   > APIs > Zendesk API.
   - Enable 'Token access'.
   - Add an API token.

3. On the Zendesk site, configure Zendesk's SSO at Account > Settings >
   Security > Single Sign-On:
  - Enable the "JSON Web Token" strategy.
  - Insert the remote authentication url: https://yoursite.com/services/zendesk
    Note that Zendesk requires the URL's protocol to be https. This will work
    even if your site uses http.
  - Optionally you can insert your logout url: http://yoursite.com/logout
  - Save the configuration and copy the shared secret.

4. At Administration > Configuration > Web services > Zendesk remote
   authentication settings, fill in:
   - The url of your Zendesk support page (e.g. http://yourdomain.zendesk.com)
   - The API token.
   - The secret key.

## Local Development

Zendesk remote authentication does not require your site to be public.

## Credits

Originally developed for 6.x by twom <http://drupal.org/user/25564>

Ported to Drupal 7.x by markwk <http://drupal.org/user/1094790>

Ported to Drupal 9.x by sumeshsr <http://drupal.org/user/3516919>
