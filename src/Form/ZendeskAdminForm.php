<?php

namespace Drupal\zendesk\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Zendesk admin settings form for this site.
 */
class ZendeskAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['zendesk.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'zendesk_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('zendesk.settings');
    $form['zendesk']['zendesk_api'] = [
      '#type' => 'fieldset',
      '#title' => 'API configuration',
    ];

    $form['zendesk']['zendesk_api']['zendesk_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Zendesk URL'),
      '#description' => 'The url of your Zendesk support page (e.g. http://yourdomain.zendesk.com).',
      '#default_value' => $config->get('zendesk_url'),
    ];

    $form['zendesk']['zendesk_api']['zendesk_api_token'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Zendesk API Token'),
      '#description' => $this->t('Use the http://www.yourdomain.com/agent/admin/api/settings page in your Zendesk configuration page. (Go to Account -> Channels)'),
      '#default_value' => $config->get('zendesk_api_token'),
    ];

    $form['zendesk']['zendesk_api']['zendesk_api_mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mail address of the API user'),
      '#default_value' => $config->get('zendesk_api_mail'),
      '#description' => $this->t('This is typically the mail address of the Zendesk admin account.'),
    ];

    $role_options = [];
    // Role-based visibility settings.
    foreach (user_roles() as $machine_name => $role_object) {
      $role_options[$machine_name] = $role_object->id();
    }

    $form['zendesk']['zendesk_permissions'] = [
      '#type' => 'fieldset',
      '#description_display' => 'before',
      '#description' => $this->t('Restrict access to Zendesk based on user roles. These rules will apply for both user synchronization and remote authentication.'),
    ];

    $form['zendesk']['zendesk_permissions']['zendesk_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Authenticate only for specific roles'),
      '#default_value' => $config->get('zendesk_roles') ?? [],
      '#options' => $role_options,
      '#description' => $this->t('Select which roles may be authenticated for zendesk. If you select no roles, all authenticated Drupal users will be authenticated for Zendesk.'),
    ];

    $form['zendesk']['zendesk_permissions']['zendesk_no_permission_page'] = [
      '#type' => 'url',
      '#title' => $this->t('No permission page'),
      '#default_value' => $config->get('zendesk_no_permission_page'),
      '#description' => $this->t('The path that users who have no permission to access Zendesk should be redirected to.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('zendesk.settings')
      ->set('zendesk_url', $form_state->getValue('zendesk_url'))
      ->set('zendesk_api_token', $form_state->getValue('zendesk_api_token'))
      ->set('zendesk_api_mail', $form_state->getValue('zendesk_api_mail'))
      ->set('zendesk_roles', $form_state->getValue('zendesk_roles'))
      ->set('zendesk_no_permission_page', $form_state->getValue('zendesk_no_permission_page'))
      ->save();

    return parent::submitForm($form, $form_state);
  }

}
