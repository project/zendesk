<?php

namespace Drupal\zendesk\Utils;

/**
 * The Zendesk Helper.
 */
class ZendeskHelper {

  /**
   * Converts and signs a PHP object or array into a JWT string.
   *
   * Taken from PEAR::JWT.
   *
   * @param object|array $payload
   *   PHP object or array.
   * @param string $key
   *   The secret key.
   *
   * @return string
   *   A signed JWT.
   */
  public static function jwtEncode($payload, $key) {
    $header = [
      'typ' => 'JWT',
      'alg' => 'HS256',
    ];
    $segments = [];
    $segments[] = self::urlSafeB64Encode(json_encode($header));
    $segments[] = self::urlSafeB64Encode(json_encode($payload));
    $signing_input = implode('.', $segments);

    $signature = hash_hmac('sha256', $signing_input, $key, TRUE);
    $segments[] = self::urlSafeB64Encode($signature);

    return implode('.', $segments);

  }

  /**
   * Encodes the given data with urlsafe base64.
   *
   * A base64 encoded string is made urlsafe by replacing '+' with '-',
   * '/' with '_', and removing '='.
   *
   * Taken from PEAR::JWT.
   *
   * @param string $data
   *   The data to encode.
   *
   * @return string
   *   The encoded string.
   */
  public static function urlSafeB64Encode($data) {
    $b64 = base64_encode($data);

    return str_replace(['+', '/', '\r', '\n', '='], ['-', '_'], $b64);
  }

}
