<?php

namespace Drupal\zendesk_sso\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\zendesk\Utils\ZendeskHelper;

/**
 * Controller routines for zendesk routes.
 */
class ZendeskSSOController extends ControllerBase {

  /**
   * Remote authentication script.
   *
   * @see http://developer.zendesk.com/documentation/sso
   * @see https://support.zendesk.com/entries/23675367
   */
  public function sso() {
    // Redirect anonymous user to login with destination.
    $account = $this->currentUser();
    if ($account->id()) {
      // Check if user role is allowed to be authenticated.
      if (\Drupal::service('zendesk_user.manager')->hasAccess($account)) {

        $token = [
          'jti' => sha1($account->id() . \Drupal::time()->getRequestTime() . rand()),
          'iat' => \Drupal::time()->getRequestTime(),
          'name' => $account->getDisplayName(),
          'email' => $account->getEmail(),
          'external_id' => $account->id(),
        ];
        $key = \Drupal::config('zendesk.settings')
          ->get('zendesk_jwt_shared_secret');
        $jwt = ZendeskHelper::jwtEncode($token, $key);

        // Redirect.
        $url = \Drupal::config('zendesk.settings')->get('zendesk_url') . '/access/jwt';

        $redirect_url = Url::fromUri($url, ['query' => ['jwt' => $jwt]]);

        // User is logged in and has permission to log in to Zendesk: redirect
        // to Zendesk with the credentials.
        $response = new TrustedRedirectResponse($redirect_url->toString());
        // Do not cache this response: Zendesk requires the 'iat' parameter to
        // be the current time to within a small window.
        $response->getCacheableMetadata()->setCacheMaxAge(0);
        return $response;
      }
      else {
        // User is logged in but has no permission to log in to Zendesk:
        // redirect to the no permission page.
        $no_permission_url_config = \Drupal::config('zendesk.settings')->get('zendesk_no_permission_page');
        if ($no_permission_url_config) {
          $no_permission_url = Url::fromUri($no_permission_url_config);
        }
        else {
          $no_permission_url = Url::fromRoute('<front>');
        }
        return new RedirectResponse($no_permission_url->toString());
      }
    }
    else {
      // User is anonymous: redirect to the regular user login page, with a
      // destination query parameter that will return the user here when they
      // have logged in.
      $redirect_url = Url::fromUri('internal:/user/login', ['query' => ['destination' => 'services/zendesk']], ['absolute' => TRUE]);

      return new RedirectResponse($redirect_url->toString());
    }
  }

}
