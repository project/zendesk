<?php

namespace Drupal\zendesk_users;

use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Utility\Error;
use Psr\Log\LoggerInterface;
use Zendesk\API\Exceptions\ApiResponseException;
use Zendesk\API\Exceptions\AuthException;
use Zendesk\API\HttpClient as ZendeskAPI;

/**
 * The Zendesk Users Service.
 */
class ZendeskUsers {
  /**
   * The database object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The zendesk library client.
   *
   * @var \Zendesk\API\HttpClient
   */
  protected $zendeskClient;

  /**
   * Constructs a new DrupaliseMe object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Base Database API class.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Interface for classes that manage a set of enabled modules.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Defines the interface for a configuration object factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(Connection $connection, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory, LoggerInterface $logger) {
    $this->database = $connection;
    $this->moduleHandler = $module_handler;
    $this->config = $config_factory->get('zendesk.settings');
    $this->logger = $logger;
    $this->zendeskClient = $this->initLibrary();
  }

  /**
   * Initialize the API Call.
   */
  protected function initLibrary() {
    $token = $this->config->get('zendesk_api_token');
    $user = $this->config->get('zendesk_api_mail');
    $domain = parse_url($this->config->get('zendesk_url'));

    $client = new ZendeskAPI('', $user, $domain['scheme'], $domain['host']);
    try {
      $client->setAuth('basic', [
        'username' => $user,
        'token' => $token,
      ]);
    }
    catch (AuthException $e) {
      $this->handleError('Authentication error initialising library', $e);
    }

    return $client;
  }

  /**
   * Creates a user in zendesk.
   */
  public function createUser(AccountInterface $account) {
    if ($this->config->get('zendesk_api_sync_users') && $this->hasAccess($account)) {

      $data = [
        'name' => $account->getDisplayName(),
        'email' => $account->getEmail(),
        'role' => 'end-user',
      ];

      if ($this->config->get('zendesk_authed_user')) {
        $data['verified'] = TRUE;
      }

      // Invoke a alter call to allow other modules to pass data to ZenDesk.
      $this->moduleHandler->alter(['zendesk_user', 'zendesk_user_update'], $data, $account);

      // Make the call.
      try {
        $result = $this->zendeskClient->users()->create($data);
      }
      catch (AuthException $e) {
        $this->handleError('Authentication error from Zendesk when creating user', $e);

        return FALSE;
      }
      catch (ApiResponseException $e) {
        $error_details = $e->getErrorDetails();
        // Docs for AuthException::getErrorDetails() are incorrect; this is a
        // JSON string not an array.
        $error_data = json_decode($error_details);

        // Try to handle special case where a user can be on zendesk's side, but
        // not recorded on our table.
        if ($error_data->error == 'RecordInvalid' && $error_data->details->email[0]->description == 'Email: ' . $account->getEmail() . ' is already being used by another user') {
          return $this->syncUserBack($account);
        }

        $this->handleError('API error from Zendesk when creating user', $e);

        return FALSE;
      }

      // @todo Not sure if this is still needed with the 2.x version of the
      // Zendesk library.
      if (!empty($result->error)) {
        $this->logger->error($result->description . ': ' . print_r($result->details, TRUE));

        return FALSE;
      }
      else {
        $this->database->insert('zendesk_users')
          ->fields([
            'uid' => $account->id(),
            'zid' => $result->user->id,
          ])
          ->execute();

        return $result->user->id;
      }
    }
  }

  /**
   * Syncs a Drupal account if Zendesk has a matching account.
   *
   * This searches Zendesk for a user matching the given Drupal account, and if
   * one is found, links the two in our data.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The Drupal account to synchronize.
   *
   * @return int|false
   *   The Zendesk user ID if a matching user was found on Zendesk, FALSE if
   *   no matching user was found.
   */
  public function syncUserBack(AccountInterface $account) {
    // Look for the user.
    $result = $this->zendeskClient->users()->search(['query' => $account->getEmail()]);

    if (empty($result->users)) {
      $result = $this->zendeskClient->users()->search(['query' => $account->getDisplayName()]);
    }

    if (isset($result->users[0]->id)) {
      $this->database->insert('zendesk_users')
        ->fields([
          'uid' => $account->id(),
          'zid' => $result->users[0]->id,
        ])
        ->execute();

      return $result->users[0]->id;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Updates the user data on zendesk.
   */
  public function updateUser(AccountInterface $account) {
    if ($this->config->get('zendesk_api_sync_users') && $this->hasAccess($account)) {
      if ($zendesk_user_id = $this->getZendeskUserId($account->id())) {
        $data = [
          'id' => $zendesk_user_id,
          'name' => $account->getDisplayName(),
          'email' => $account->getEmail(),
        ];

        // If the user's email has changed, mark the new email as verified.
        // Note that a changed email is added in Zendesk as a secondary email;
        // Zendesk does not change the primary email addres: see
        // https://developer.zendesk.com/api-reference/ticketing/users/users/#update-user.
        if ($this->config->get('zendesk_authed_user')) {
          $data['verified'] = TRUE;
        }
      }
      elseif ($zendesk_user_id = $this->syncUserBack($account)) {
        $data = [
          'id' => $zendesk_user_id,
          'name' => $account->getDisplayName(),
          'email' => $account->getEmail(),
        ];

        if ($this->config->get('zendesk_authed_user')) {
          $data['verified'] = TRUE;
        }
      }
      else {
        // There is no matching user on Zendesk: do nothing.
        // @todo Decide whether we should create the user in Zendesk as this
        // point: see https://www.drupal.org/project/zendesk/issues/3344714.
        return;
      }

      // Invoke a alter call to allow other modules to pass data to ZenDesk.
      $this->moduleHandler->alter(['zendesk_user', 'zendesk_user_update'], $data, $account);

      // Make the call.
      try {
        $this->zendeskClient->users()->update($zendesk_user_id, $data);
      }
      catch (AuthException $e) {
        $this->handleError('Authentication error from Zendesk when updating user', $e);
      }
      catch (ApiResponseException $e) {
        $this->handleError('API error from Zendesk when updating user', $e);
      }
    }
  }

  /**
   * Delete the user from zendesk.
   */
  public function deleteUser(AccountInterface $account) {
    if ($this->config->get('zendesk_api_sync_users') && $zendesk_user_id = $this->getZendeskUserId($account->id())) {
      // Make the call.
      try {
        $this->zendeskClient->users()->delete($zendesk_user_id);
      }
      catch (AuthException $e) {
        $this->handleError('Authentication error from Zendesk when deleting user', $e);
      }
      catch (ApiResponseException $e) {
        $this->handleError('API error from Zendesk when deleting user', $e);
      }

      // Delete user entry from the zendesk users table.
      $this->database->delete('zendesk_users', ['uid' => $account->id()])->execute();
    }
  }

  /**
   * Helper function to retrieve Zendesk ID of the user.
   *
   * @param integer $uid
   *   The Drupal user ID to get the Zendesk user ID for.
   *
   * @return int
   *   The Zendesk user ID.
   */
  public function getZendeskUserId(int $uid) {
    $result = $this->database->select('zendesk_users', 'zu')
      ->fields('zu', ['uid', 'zid'])
      ->condition('zu.uid', $uid, '=')
      ->execute();
    $rows = $result->fetchAll();
    if (count($rows) <> 0) {
      foreach ($rows as $user) {
        return $user->zid;
      }
    }
    else {
      return FALSE;
    }

  }

  /**
   * Checks if the user has access based on config.
   */
  public function hasAccess($account) {
    $zendesk_roles = $this->config->get('zendesk_roles');
    if (!array_sum($zendesk_roles)) {
      // No roles are set, give access.
      return TRUE;
    }
    else {
      $keys = array_keys($account->getRoles);
      foreach ($keys as $key) {
        if ($zendesk_roles[$key] > 0) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * Logs an exception from Zendesk.
   *
   * @param string $message
   *   A message for the log. Omit the final full stop.
   * @param \Exception $exception
   *   The exception from the Zendesk library.
   */
  protected function handleError(string $message, \Exception $exception): void {
    $backtrace = $exception->getTrace();
    $caller = Error::getLastCaller($backtrace);

    $this->logger->error('@message in %function (line %line of %file) @backtrace_string.', [
      '@message' => $message,
      'backtrace' => $backtrace,
      '@backtrace_string' => $exception->getTraceAsString(),
      'exception' => $exception,
      '%file' => $caller['file'],
      '%line' => $caller['line'],
      '%function' => $caller['function'],
    ]);
  }

}
