<?php

/**
 * @file
 * Hooks provided by the Zendesk Users module.
 */

/**
 * Alter the user data on the fly, when a user is created/updated.
 *
 * NOTE: This hook only works if the zendesk user synchronization is enabled.
 * /admin/config/system/zendesk.
 *
 * @param array $data
 *   Zendesk userdata.
 * @param \Drupal\Core\Session\AccountInterface $account
 *   Object of the user.
 */
function hook_zendesk_user(array &$data, \Drupal\Core\Session\AccountInterface $account) {
}
